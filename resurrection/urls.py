from django.urls import include, path
from django.conf.urls.static import static
from rest_framework import routers
from rest_framework.authtoken import views as token_views
from rest_framework.schemas import get_schema_view

from app import views
from resurrection import settings

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'regions', views.RegionViewSet)
router.register(r'departments', views.DepartmentViewSet)
router.register(r'teams', views.TeamViewSet)
router.register(r'teamusers', views.TeamUserViewSet)
router.register(r'sglists', views.SgListViewSet)


urlpatterns = [
    path('', include(router.urls)),
    
    # POST with {"username": "xx", "password": "xx"}
    # Ensuite, ajouter le header suivant
    # Authorization: Token [TOKEN_ID]
    path('api-token-auth/', token_views.obtain_auth_token),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)