export default class User {
    static from (token) {
        if(token && token.startsWith('Token ')) {
            return new User(token)
        }
        else {
            return null
        }
    }

    constructor ({ token }) {
        this.token = token
        this.admin = true
    }

    get isAdmin () {
        return this.admin
    }
}