from django.db import models

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import AbstractUser


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Region(models.Model):
    code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)

    def __str__(self):
        return '{} - {}'.format(self.code, self.name)


class Department(models.Model):
    code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, related_name='departments', null=True)

    def __str__(self):
        return '{} - {}'.format(self.code, self.name)


class User(AbstractUser):
    NEW = 'G1'
    POSITIONED = 'G2'
    BANISHED = 'G3'

    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='users', null=True)
    state = models.CharField(
        max_length=2,
        choices=[(NEW, 'Nouvel inscrit'), (POSITIONED, "Positionné"), (BANISHED, "Banni")],
        default=NEW
    )

    def __str__(self):
        return self.username


class Team(models.Model):
    name = models.CharField(max_length=100)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='teams')

    def __str__(self):
        return '{} - {}'.format(self.department.__str__(), self.name)


class TeamUser(models.Model):
    SOLDAT = 'G1'
    CHIEF  = 'G2'
    NEW    = 'G3'

    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='users')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grade = models.CharField(
        max_length=2,
        choices=[(SOLDAT, 'Soldat'), (CHIEF, "Chef d'équipe"), (NEW, "Nouveau dans l'équipe")],
        default=NEW
    )


class SgList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    list_id = models.CharField(max_length=10)