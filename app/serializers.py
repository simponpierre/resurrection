from django.contrib.auth.models import Group
from rest_framework import serializers
from app import models as m


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = m.User
        fields = ['url', 'department', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class RegionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = m.Region
        fields = ['url', 'code', 'name']


class DepartmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = m.Department
        fields = ['url', 'code', 'name', 'region']


class TeamUserSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = m.TeamUser
        fields = ['team', 'user', 'grade']


class TeamSerializer(serializers.HyperlinkedModelSerializer):
    users = TeamUserSerializer(many=True)

    class Meta:
        model = m.Team
        fields = ['url', 'name', 'department', 'users']


class SgListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = m.SgList
        fields = ['url', 'user', 'list_id']