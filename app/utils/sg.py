import pycurl
from urllib.parse import urlencode
from io import BytesIO
import json


class Sg():
    def __init__(self, member_id, activation_code):
        self.member_id = member_id
        self.activation_code = activation_code
        self.api_url = 'https://sg-autorepondeur.com/API_V2/'
    
    def _init_data(self, action, data):
        data['membreid'] = self.member_id
        data['codeactivation'] = self.activation_code
        data['action'] = action
    
    def _call(self, action, data={}):
        self._init_data(action, data)

        buffer = BytesIO()
        c = pycurl.Curl()
        c.setopt(c.URL, self.api_url)
        c.setopt(c.POST, True)
        c.setopt(c.POSTFIELDS, urlencode(data))
        c.setopt(c.WRITEDATA, buffer)
        try:
            c.perform()
        except:
            raise Exception("Can't join sg")

        c.close()
        body = buffer.getvalue()
        ret = json.loads(body)

        if not ret['valid']:
            raise Exception("Invalid response from SG: "+ret.__str__())

        return ret['reponse']
    
    def get_lists(self):
        # We only want R1 and R2 lists
        # listeid, nom
        values = self._call('get_list')
        res_lists = [x for x in values if x['nom'].startswith('R2') or 'Renversement' in x['nom']]
        return res_lists
    
    def get_users_in_list(self, list_id):
        # id, prenom, email, actif (0 non actif, 1 actif, 2  resilie, 5 invalide)
        return self._call('get_subscriber', {'listeid': list_id, 'details': True})
    
    def add_user_in_list(self, list_id, pseudo, email, department):
        data = {'listeid': list_id, 'email': email, 'pseudo': pseudo, 'codepostal': department}
        self._call('set_subscriber', data)
    
    def del_user_from_list(self, list_id, email):
        self._call('del_subscriber', {'listeid': list_id, 'email': email, 'force': True})