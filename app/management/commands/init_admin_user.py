from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError
import environ

env = environ.Env()


class Command(BaseCommand):
    help = 'Init admin user'

    def handle(self, *args, **options):
        try:
            username = env("IDENTITY_ADMIN_USERNAME")
            password = env("IDENTITY_ADMIN_PASSWORD")
            get_user_model().objects.create_superuser(
                username=username,
                password=password,
                email='admin@email.com')
        except IntegrityError:
            print('Admin user already created')
        else:
            print('Admin user created')
