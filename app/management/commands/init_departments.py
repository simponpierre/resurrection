import json
from os import path
from django.core.management.base import BaseCommand
from app.models import Department, Region


class Command(BaseCommand):
    help = 'Populate department'

    def handle(self, *args, **options):
        base_path = path.dirname(path.abspath(__file__))
        json_path = path.join(base_path, 'departments_regions_france_2016.json')
        with open(json_path, 'r') as f:
            data = json.loads(f.read())
        
        nb_created_departments = 0
        for d in data:
            department_code = d['departmentCode']
            department_name = d['departmentName']
            region_code = d['regionCode']
            region_name = d['regionName']

            if region_code and region_name:
                try:
                    region = Region.objects.get(code=region_code)
                except Region.DoesNotExist:
                    region = Region(code=region_code, name=region_name)
                    region.save()
            else:
                region = None

            try:
                department = Department.objects.get(code=department_code)
            except Department.DoesNotExist:
                department = Department(code=department_code, name=department_name, region=region)
                department.save()
                nb_created_departments += 1
        
        print(f"Init department: {nb_created_departments} new departments created")
        