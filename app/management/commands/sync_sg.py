import threading

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from app.utils.sg import Sg
from app.models import User, SgList


class Command(BaseCommand):
    help = 'Sync with Sg-Autorepondeur'
    
    def __init__(self):
        self.c1 = 0
        self.c2 = 0

    def sync_list_down(self, sg, l):
        users = sg.get_users_in_list(l['listeid'])
        for u in users:
            user_email = u['email']
            user_name = u['prenom']
            list_id = l['listeid']
            try:
                local_user = User.objects.get(email=user_email)
            except User.DoesNotExist:
                local_user = User(department=None, state=User.NEW, username=user_name, email=user_email)
                local_user.save()
                self.c1 += 1
            
            try:
                local_user_list = SgList.objects.get(user=local_user, list_id=list_id)
            except SgList.DoesNotExist:
                local_user_list = SgList(user=local_user, list_id=list_id)
                local_user_list.save()
                self.c2 += 1

    def sync_down(self, sg):
        lists = sg.get_lists()
        self.c1 = 0
        self.c2 = 0
        threads = []
        for l in lists:
            threads.append(threading.Thread(target=self.sync_list_down, args=(sg, l)))

        for t in threads:
            t.start()
        
        for t in threads:
            t.join()
        
        if self.c1 or self.c2:
            print("New users: {} / New users in list: {}".format(self.c1, self.c2))

    def handle(self, *args, **options):
        sg = Sg(settings.SG_MEMBER_ID, settings.SG_ACTIVATION_CODE)
        self.sync_down(sg)