import threading
import csv

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from app.utils.sg import Sg


class Command(BaseCommand):
    help = 'Download all sg users'
    
    def __init__(self):
        # array of: [
        # 'email', 'listeid', 'nom', 'prenom', 'adresse',
        # 'codepostal', 'ville', 'pseudo', 'dateactif'=2020-03-15,
        # 'anneeorigine', 'moisorigine', 'jourorigine'
        # ]
        self.all_users = []

    def sg_list_down(self, sg, l):
        print("Retrieve users of list {}".format(l['listeid']))
        users = sg.get_users_in_list(l['listeid'])
        for u in users:
            self.all_users.append(u)

    def sg_down(self, sg):
        print("Retrieve lists")
        lists = sg.get_lists()
        threads = []
        for l in lists:
            threads.append(threading.Thread(target=self.sg_list_down, args=(sg, l)))

        for t in threads:
            t.start()
        
        for t in threads:
            t.join()

    def handle(self, *args, **options):
        sg = Sg(settings.SG_MEMBER_ID, settings.SG_ACTIVATION_CODE)
        self.sg_down(sg)

        result = {}
        for raw_user in self.all_users:
            email = raw_user['email']
            list_id = raw_user['listeid']

            if email in result:
                result[email]['lists'].append(list_id)
                continue
            
            # pseudo
            pseudo = raw_user['pseudo']
            if not pseudo:
                pseudo = raw_user['prenom'] + ' ' + raw_user['nom']
            if not pseudo:
                print('Attention Pas de pseudo')
            
            # postal code
            postal_code = raw_user['codepostal']
            if not postal_code:
                postal_code = raw_user['adresse']
            if not postal_code:
                postal_code = raw_user['ville']

            # dates
            date_register = '-'.join([raw_user['anneeorigine'], raw_user['moisorigine'], raw_user['jourorigine']])
            date_last_active = raw_user['dateactif']

            user = {
                'lists': [list_id],
                'pseudo': pseudo,
                'postal_code': postal_code,
                'date_register': date_register,
                'date_last_active': date_last_active
            }

            result[email] = user

        # write csv
        header = ['Email', 'Pseudo', 'Code postal', "Listes d'appartenance",
        "Date d'enregistrement", "Date dernière activité"]
        data = []
        for k, v in result.items():
            data.append([
                k, v['pseudo'], v['postal_code'], ','.join(v['lists']),
                v['date_register'], v['date_last_active']
            ])

        with open('./out.csv', 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(header)
            writer.writerows(data)
        
        print('File written to ./out.csv')