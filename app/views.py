from django.contrib.auth.models import Group
from app import models as m
from rest_framework import viewsets
from app import serializers as s


class UserViewSet(viewsets.ModelViewSet):
    queryset = m.User.objects.all().order_by('-date_joined')
    serializer_class = s.UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = s.GroupSerializer


class RegionViewSet(viewsets.ModelViewSet):
    queryset = m.Region.objects.all()
    serializer_class = s.RegionSerializer


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = m.Department.objects.all()
    serializer_class = s.DepartmentSerializer


class TeamViewSet(viewsets.ModelViewSet):
    queryset = m.Team.objects.all()
    serializer_class = s.TeamSerializer


class TeamUserViewSet(viewsets.ModelViewSet):
    queryset = m.TeamUser.objects.all()
    serializer_class = s.TeamUserSerializer


class SgListViewSet(viewsets.ModelViewSet):
    queryset = m.SgList.objects.all()
    serializer_class = s.SgListSerializer