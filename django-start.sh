#!/bin/sh

python manage.py wait_for_db &&
python manage.py migrate &&
python manage.py collectstatic --noinput &&
python manage.py init_admin_user &&
python manage.py init_departments &&
gunicorn resurrection.wsgi:application --bind 0.0.0.0:80