# Resurrection

This module will contain all data and must be secure

## How to use

```
docker-compose up
```

This will execute the followings commands:

```
# create db
docker exec resurrection_api python manage.py migrate

# create admin user
docker exec resurrection_api python manage.py init_admin_user

# populate department
docker exec resurrection_api python manage.py init_departments
```


After each modification of the model:

```
docker exec resurrection_api python manage.py makemigrations
docker exec resurrection_api python manage.py migrate
```

## Sg to csv

```
docker exec resurrection_identity-api_1 python manage.py sg_department_csv
```