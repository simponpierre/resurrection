#!/usr/bin/env python3

import sys
import psycopg2
import environ
import time
import importlib

from twisted.application import service
from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory
from twisted.python.components import registerAdapter
from twisted.python import log
from ldaptor.interfaces import IConnectedLDAPEntry
from ldaptor.protocols.ldap.ldapserver import LDAPServer
from ldaptor.inmemory import ReadOnlyInMemoryLDAPEntry
from ldaptor.protocols.ldap import ldaperrors

from django.contrib.auth import hashers
from resurrection import settings


# To connect a user use this dn: uid=[PSEUDO],ou=people,dc=fr


SQL_CHECK_USERS = "SELECT MD5(STRING_AGG(u.username || u.password, ',' ORDER BY u.username, u.password)) FROM app_user u"
SQL_LIST_USERS = "SELECT username, password FROM app_user u"

env = environ.Env()

hasher_module = settings.PASSWORD_HASHERS[0].rsplit('.', 1)
django_hasher = getattr(importlib.import_module(hasher_module[0]), hasher_module[1])()


class ResurrectionLDAPEntry(ReadOnlyInMemoryLDAPEntry):

    def _get_first_value(self, key):
        return list(self.get(key))[0]

    # override password checking
    def _bind(self, password):
        for key in self._user_password_keys:
            digest = self._get_first_value(key)
            digest_exploded = digest.split('$')
            try:
                salt = digest_exploded[2]
            except IndexError:
                pseudo = list(self.get('uid'))[0]
                print(f"Password not acceptable for user: {pseudo}")
                break

            hashed_password = hashers.make_password(password, salt=salt, hasher=django_hasher)

            if hashed_password == digest:
                return self

        raise ldaperrors.LDAPInvalidCredentials()


class LDAPServerFactory(ServerFactory):
    protocol = LDAPServer

    def __init__(self):
        self.init_db()
        self.update_tree()
        self.last_md5 = self.get_md5()

    def init_db(self):
        host = env("IDENTITY_DATABASE_HOST")
        database = env("IDENTITY_DATABASE_NAME")
        user = env("IDENTITY_DATABASE_USER")
        port = env("IDENTITY_DATABASE_PORT")
        password = env("IDENTITY_DATABASE_PASSWORD")
        try:
            self.db_conn = psycopg2.connect(host=host, database=database, user=user, port=port, password=password)
        except psycopg2.DatabaseError as e:
            print(f'Error {e}')
            sys.exit(1)

    def get_md5(self):
        cur = self.db_conn.cursor()
        cur.execute(SQL_CHECK_USERS)
        return cur.fetchone()[0]

    def check_tree(self):
        md5 = self.get_md5()
        
        if md5 != self.last_md5:
            print("Reloading tree")
            self.update_tree()
            self.last_md5 = md5

    def update_tree(self):
        new_tree = ResurrectionLDAPEntry(
            dn = "dc=fr",
            attributes = {
                "objectClass": ["dcObject", "country"],
                "dc": ["fr"],
                "description": ["French country 2 letters iso description"],
            },
        )

        people = new_tree.addChild(
            rdn = "ou=people",
            attributes = {
                "ou": ["people"],
                "description": ["People"],
                "objectClass": ["organizationalunit"],
            }
        )

        cur = self.db_conn.cursor()
        cur.execute(SQL_LIST_USERS)
        for row in cur.fetchall():
            pseudo = row[0]
            password = row[1]

            people.addChild(
                rdn = f"uid={pseudo}",
                attributes = {
                    "objectClass": ["people", "inetOrgPerson"],
                    "uid": [pseudo],
                    "userPassword": [password],
                    "cn": [pseudo],
                    "sn": [pseudo],
                    "givenName": [pseudo],
                    "mail": [f"/home/{pseudo}/mailDir"]
                }
            )

        self.tree = new_tree

    def get_root(self):
        self.check_tree()
        return self.tree

    def buildProtocol(self, addr):
        proto = self.protocol()
        proto.debug = self.debug
        proto.factory = self
        return proto


def wait_for_db():
    host = env("IDENTITY_DATABASE_HOST")
    database = env("IDENTITY_DATABASE_NAME")
    user = env("IDENTITY_DATABASE_USER")
    password = env("IDENTITY_DATABASE_PASSWORD")

    for nb_try in range(5): 
        try:
            db_conn = psycopg2.connect(host=host, database=database, user=user, password=password)
            cur = db_conn.cursor()
            cur.execute(SQL_LIST_USERS)
            db_conn.close()
            break
        except:
            print(f"Database not ready, waiting... Try {nb_try}")
            time.sleep(3)
    else:
        raise Exception("Error: Database not ready")
    

if __name__ == "__main__":
    wait_for_db()

    if len(sys.argv) == 2:
        port = int(sys.argv[1])
    else:
        port = 389

    log.startLogging(sys.stderr)
    registerAdapter(lambda x: x.get_root(), LDAPServerFactory, IConnectedLDAPEntry)
    factory = LDAPServerFactory()
    factory.debug = True
    application = service.Application("ldaptor-server")
    myService = service.IServiceCollection(application)
    reactor.listenTCP(port, factory)
    reactor.run()
